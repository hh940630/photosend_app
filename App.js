import React, { Component } from "react";
import { StatusBar } from "react-native";
// import { Root } from "native-base";
import { AppLoading } from "expo";
// import * as Font from "expo-font";

import RootNavigation from "./src/Navigator/RootNavigation";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  handleError = error => console.log(error);

  handleLoaded = () => this.setState({ loaded: true });

  loadAssets = async () => {
    //   // Font Preloading
    //   await Font.loadAsync({
    //     Roboto: require("native-base/Fonts/Roboto.ttf"),
    //     Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
    //     "Sweet Sensations Persona Use": require("./assets/fonts/Sweet_Sensations_Persona_Use.ttf"),
    //     "noto-sans": require("./assets/fonts/NotoSansKR-Regular.otf"),
    //     "noto-sans-bold": require("./assets/fonts/NotoSansKR-Bold.otf")
    //   });

    console.log("loadAssets complete!!!");
  };

  render() {
    const { loaded } = this.state;

    return (
      // <Provider store={store}>
      //   <RootNavigator />
      // </Provider>

      <>
        <StatusBar barStyle="dark-content" />
        {loaded ? (
          <RootNavigation />
        ) : (
          <AppLoading
            startAsync={this.loadAssets}
            onFinish={this.handleLoaded}
            onError={this.handleError}
          />
        )}
      </>
    );
  }
}
