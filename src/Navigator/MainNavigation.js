import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import Icon from "react-native-vector-icons/Ionicons";

// screen
import Home from "../screens/mainScreen/Home";
import List from "../screens/mainScreen/List";
import Blank from "../screens/mainScreen/Blank";
import Cart from "../screens/mainScreen/Cart";
import MY from "../screens/mainScreen/MY";

// import PaymentScreen from "../screens/Payment1";

// const CartStack = createStackNavigator(
//   {
//     Cart,
//     PaymentScreen
//   },
//   {
//     defaultNavigationOptions: ({ navigation }) => ({
//       header: null
//     }),
//     initialRouteName: "Cart"
//   }
// );

const MainNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-home" style={{ color: tintColor, fontSize: 28 }} />
        )
      }
    },
    List: {
      screen: List,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-menu" style={{ color: tintColor, fontSize: 28 }} />
        )
      }
    },
    Blank: {
      screen: Blank,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="ios-square-outline"
            style={{ color: tintColor, fontSize: 28 }}
          />
        ),
        showLabel: false
      }
    },
    Cart: {
      screen: Cart,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-basket" style={{ color: tintColor, fontSize: 28 }} />
        )
      }
    },
    My: {
      screen: MY,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-person" style={{ color: tintColor, fontSize: 28 }} />
        )
      }
    }
  },
  {
    tabBarOptions: {
      animationEnabled: true,
      swipeEnabled: true,
      activeTintColor: "#634BCF", // identity color
      inactiveTintColor: "#909090",
      style: {
        backgroundColor: "white",
        borderTopWidth: 0,
        height: 60,
        paddingBottom: 10
      },
      showLabel: true,
      showIcon: true
    }
  }
);

export default MainNavigator;
