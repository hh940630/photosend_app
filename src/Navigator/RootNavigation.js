import { createSwitchNavigator, createAppContainer } from "react-navigation";

import IntroScreen from "../screens/IntroScreen";
import SignInScreen from "../screens/SignInScreen";
import SignUpScreen from "../screens/SignUpScreen";
import InitializingScreen from "../screens/InitializingScreen";
import MainNavigation from "./MainNavigation";

const RootNavigation = createSwitchNavigator(
  {
    Intro: {
      screen: IntroScreen,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    Initializing: {
      screen: InitializingScreen,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    SignIn: {
      screen: SignInScreen,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    Main: {
      screen: MainNavigation,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    }
  },
  {
    // initialRouteName: "Intro"  // pre
    initialRouteName: "Main"
  }
);

export default createAppContainer(RootNavigation);
