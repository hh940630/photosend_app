import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Modal,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import ScreenTitle from "../../Components/ScreenTitle";
import SetItem from "../../Components/SetItem";
import ItemAddBtn from "../../Components/ItemAddBtn";
import Payment1 from "../Payment/Payment1";
import Payment3 from "../Payment/Payment3";
import Payment4 from "../Payment/Payment4";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      check1: false,
      check2: false,
      check3: false,
      check4: false
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  _selectCheck1 = () => {
    this.setState({ check1: true });
  };

  _selectCheck2 = () => {
    this.setState({ check2: true });
  };

  _selectCheck3 = () => {
    this.setState({ check3: true });
  };

  _selectCheck4 = () => {
    this.setState({ check4: true });
  };

  render() {
    // const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <ScreenTitle />

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingTop: 18
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity>
              <Icon
                name="ios-radio-button-off"
                style={{ fontSize: 20, color: "#5C5C5C" }}
              ></Icon>
            </TouchableOpacity>
            <Text style={{ paddingLeft: 4 }}>Total</Text>
          </View>
          <TouchableOpacity style={styles.deleteBtn}>
            <Text style={{ fontSize: 12, padding: 2 }}>删掉(삭제)</Text>
          </TouchableOpacity>
        </View>

        <View style={{ flex: 3 }}>
          <ScrollView
            style={{
              marginTop: 18,
              marginBottom: 18,
              maxHeight: 280
            }}
          >
            <SetItem />

            <ItemAddBtn />
            <ItemAddBtn />
          </ScrollView>

          <View style={styles.itemPriceCalcuation}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                paddingBottom: 12
              }}
            >
              <Text>总商品⾦额⾦额(1件)</Text>
              <Text>￥ 300</Text>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text>运费</Text>
              <Text>￥ 120</Text>
            </View>
          </View>
          <View style={styles.itemTotalPrice}>
            <Text style={{ fontSize: 14, fontWeight: "bold" }}>结算总金额</Text>
            <Text
              style={{ fontSize: 14, fontWeight: "bold", color: "#FF6969" }}
            >
              ￥ 420
            </Text>
          </View>

          <View>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                // Alert.alert("Modal has been closed.");
              }}
            >
              <View style={{ marginTop: 22 }}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    paddingTop: 30,
                    paddingBottom: 12,
                    paddingLeft: 22,
                    paddingRight: 22,
                    backgroundColor: "#5747C3"
                  }}
                >
                  <TouchableOpacity style={{ alignItems: "flex-start" }}>
                    <Icon
                      name="md-arrow-back"
                      style={{ fontSize: 20, color: "#FFF" }}
                    ></Icon>
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 24,
                      color: "#FFF",
                      alignItems: "center"
                    }}
                  >
                    订单
                  </Text>
                  <Text> </Text>
                </View>

                <ScrollView
                  style={{
                    maxHeight: 550,
                    marginLeft: 22,
                    marginRight: 22,
                    marginTop: 20,
                    marginBottom: 20
                  }}
                >
                  <View style={styles.specification}>
                    <Text style={styles.specification_title}>结帐总金额</Text>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginBottom: 8
                      }}
                    >
                      <Text>总商品⾦额⾦额(1件）</Text>
                      <Text>￥ 300</Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginBottom: 8
                      }}
                    >
                      <Text>运费</Text>
                      <Text>￥ 120</Text>
                    </View>
                    <TouchableOpacity style={{ marginBottom: 14 }}>
                      <Text style={{ color: "#C6C6C6" }}>优惠券</Text>
                    </TouchableOpacity>

                    <View
                      style={{
                        borderTopWidth: 1,
                        borderTopColor: "#DDDDDD",
                        borderBottomWidth: 1
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          paddingTop: 14
                        }}
                      >
                        <Text style={{ fontWeight: "bold" }}>结帐总金额</Text>
                        <Text style={{ fontWeight: "bold", color: "#FF6969" }}>
                          ￥ 420
                        </Text>
                      </View>
                      <View style={styles.couponInput}>
                        <TextInput
                          placeholder="优惠券编号"
                          style={styles.couponInput_input}
                        ></TextInput>
                        <TouchableOpacity style={styles.couponInput_button}>
                          <Text style={{ color: "#FFF" }}>确认</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderTopWidth: 1, borderBottomWidth: 1 }}>
                    <Text
                      style={{ fontSize: 20, marginTop: 30, marginBottom: 30 }}
                    >
                      配送基本信息
                    </Text>
                    <Text style={{ marginBottom: 8 }}>可取货时间</Text>
                    <View style={{ flexDirection: "row", marginBottom: 8 }}>
                      <TouchableOpacity onPress={this._selectCheck1}>
                        <Icon
                          name="ios-radio-button-off"
                          style={{
                            fontSize: 20,
                            color: "#C6C6C6",
                            marginRight: 16
                          }}
                        ></Icon>
                      </TouchableOpacity>
                      <Text>仁川国际机场 一航厦</Text>
                    </View>
                    <View style={{ flexDirection: "row", marginBottom: 8 }}>
                      <TouchableOpacity onPress={this._selectCheck1}>
                        <Icon
                          name="ios-radio-button-off"
                          style={{
                            fontSize: 20,
                            color: "#C6C6C6",
                            marginRight: 16
                          }}
                        ></Icon>
                      </TouchableOpacity>
                      <Text>仁川国际机场 二航厦</Text>
                    </View>
                    <View style={{ flexDirection: "row", marginBottom: 8 }}>
                      <TouchableOpacity onPress={this._selectCheck3}>
                        <Icon
                          name="ios-radio-button-off"
                          style={{
                            fontSize: 20,
                            color: "#C6C6C6",
                            marginRight: 16
                          }}
                        ></Icon>
                      </TouchableOpacity>
                      <Text>韩国当地酒店 (限首尔)</Text>
                    </View>
                    <View style={{ flexDirection: "row", paddingBottom: 30 }}>
                      <TouchableOpacity onPress={this._selectCheck4}>
                        <Icon
                          name="ios-radio-button-off"
                          style={{
                            fontSize: 20,
                            color: "#C6C6C6",
                            marginRight: 16
                          }}
                        ></Icon>
                      </TouchableOpacity>
                      <Text>海外配送 (需另计运费)</Text>
                    </View>
                  </View>

                  {this.state.check1 ? <Payment1 /> : null}
                  {this.state.check2 ? <Payment1 /> : null}
                  {this.state.check3 ? <Payment3 /> : null}
                  {this.state.check4 ? <Payment4 /> : null}

                  <TouchableOpacity
                    style={styles.payment}
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#FFFFFF",
                        fontWeight: "bold"
                      }}
                    >
                      结账
                    </Text>
                  </TouchableOpacity>
                </ScrollView>
              </View>
            </Modal>

            <TouchableOpacity
              style={styles.payment}
              onPress={() => {
                this.setModalVisible(true);
              }}
            >
              <Text
                style={{ fontSize: 18, color: "#FFFFFF", fontWeight: "bold" }}
              >
                结账
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 22,
    marginRight: 22
  },
  text: {
    fontSize: 16
  },
  deleteBtn: {
    width: 64,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 8
  },
  couponInput: {
    width: 332,
    flexDirection: "row",
    height: 48,
    borderRadius: 10,
    borderColor: "#DDDDDD",
    borderWidth: 1,
    justifyContent: "center",
    marginTop: 14,
    marginBottom: 30
  },
  couponInput_input: {
    flex: 2,
    marginLeft: 14
  },
  couponInput_button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 48,
    backgroundColor: "#5747C3",
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10
  },
  itemPriceCalcuation: {
    justifyContent: "center",
    height: 80,
    borderTopWidth: 1,
    borderTopColor: "#DDDDDD",
    borderBottomWidth: 1,
    borderBottomColor: "#000"
  },
  itemTotalPrice: {
    height: 45,
    justifyContent: "space-between",
    flexDirection: "row",
    paddingTop: 15
  },
  payment: {
    height: 50,
    marginTop: 10,
    backgroundColor: "#3825B7",
    borderRadius: 28,
    justifyContent: "center",
    alignItems: "center"
  }
});
