import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage
} from "react-native";

// import { goToAuth, goHome } from "../Navigator/RegisterNavigation";

import { USER_KEY } from "../config/config";

export default class InitializingScreen extends Component {
  static navigationOptions = {
    header: null
  };

  async componentDidMount() {
    try {
      const user = await AsyncStorage.getItem(USER_KEY);
      console.log("user: ", user);
      if (user) {
        // goHome();
        this.props.navigation.navigate("Main");
      } else {
        // goToAuth();
        this.props.navigation.navigate("SignIn");
      }
    } catch (err) {
      console.log("error: ", err);
      //   goToAuth();
      this.props.navigation.navigate("SignIn");
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text>initializing(Loading)</Text>
        <TouchableOpacity
          style={{ marginTop: 30 }}
          onPress={() => navigate("Main")}
        >
          <Text>시작하기</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
