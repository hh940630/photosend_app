import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Payment3 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{ marginTop: 30, marginBottom: 8 }}>
          韩国酒店地址(请输入英文地址)
        </Text>
        <TextInput style={styles.inputHotelAddress}></TextInput>
        <Text style={{ marginBottom: 8 }}>可取货时间</Text>
        <View style={styles.selectDate}></View>
        <Text>结账方法</Text>
        <View style={styles.image_card}>
          <Image
            source={require("../../../assets/image/visa.png")}
            style={{ width: 68, height: 52 }}
          />
          <Image
            source={require("../../../assets/image/masterCard.png")}
            style={{ width: 55, height: 55 }}
          />
          <Image
            source={require("../../../assets/image/unionPay.png")}
            style={{ width: 55, height: 34 }}
          />
        </View>

        <View style={{ flexDirection: "row", marginBottom: 14 }}>
          <TouchableOpacity>
            <Icon
              name="ios-radio-button-off"
              style={{
                fontSize: 20,
                color: "#C6C6C6",
                marginRight: 16
              }}
            ></Icon>
          </TouchableOpacity>
          <Text>确认购买条件及同意结算</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputHotelAddress: {
    height: 48,
    borderColor: "#DDDDDD",
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 18
  },
  selectDate: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    height: 48,
    borderColor: "#DDDDDD",
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 18
  },
  separate: {
    height: 48,
    width: 1,
    borderColor: "#DDD",
    borderWidth: 0.5
  },
  image_card: {
    width: 230,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 30
  },
  image_cardItem: {
    width: 80
  }
});
