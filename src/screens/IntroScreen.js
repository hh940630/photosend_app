import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
  //   AsyncStorage
} from "react-native";

export default class IntroScreen extends Component {
  static navigationOptions = {
    header: null
  };

  // FIXME: 출시 전 꼭 실행시켜야함. 초기 로그인 시 App 실행시 바로 Home으로 가는 코드
  //   componentDidMount() {
  //     AsyncStorage.getItem("username").then(value => {
  //       if (value != null) {
  //         console.log(value);
  //         this.props.navigation.navigate("Main");
  //       }
  //     });
  //   }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text>Intro</Text>
        <TouchableOpacity
          style={{ marginTop: 30 }}
          onPress={() => navigate("Initializing")}
        >
          <Text>시작하기</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
