import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ flexDirection: "row", height: 100, marginBottom: 30 }}>
        {this.state.checkOn ? (
          <TouchableOpacity>
            <Icon
              name="ios-radio-button-on"
              style={{ fontSize: 20, color: "#5C5C5C" }}
            ></Icon>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity>
            <Icon
              name="ios-radio-button-off"
              style={{ fontSize: 20, color: "#5C5C5C" }}
            ></Icon>
          </TouchableOpacity>
        )}
        <View style={styles.itemImage}></View>
        <View
          style={{
            width: 170,
            justifyContent: "space-between",
            flexDirection: "column",
            marginLeft: 18
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text style={styles.itemTitle}>REEBOK ROYAL BRIDGE</Text>
            <TouchableOpacity>
              <Icon
                name="md-close"
                style={{ color: "#B4B4B4", fontSize: 22 }}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.itemPrice}>￥ 300</Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text style={styles.itemDes}>尺寸 : 250</Text>
            <Text style={styles.itemDes}>数量 : 1件</Text>
            <TouchableOpacity>
              <Icon name="ios-arrow-down" style={{ fontSize: 22 }}></Icon>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemImage: {
    width: 100,
    height: 100,
    backgroundColor: "gray",
    borderRadius: 8,
    marginLeft: 6
  },
  itemTitle: {
    width: 120,
    flexWrap: "wrap",
    height: "auto",
    fontSize: 15
  },
  itemPrice: {
    fontSize: 18
  },
  itemDes: {
    fontSize: 14
  }
});
