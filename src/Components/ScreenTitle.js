import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginTop: 30
        }}
      >
        <TouchableOpacity style={{ alignItems: "flex-start" }}>
          <Icon name="md-arrow-back" style={{ fontSize: 20 }}></Icon>
        </TouchableOpacity>
        <Text style={{ fontSize: 24, alignItems: "center" }}>购物车</Text>
        <Text> </Text>
      </View>
    );
  }
}
