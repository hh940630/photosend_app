import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.itemAddBtn}>
        <Text style={{ fontSize: 14, color: "#C6C6C6" }}>加入商品至购物车</Text>
        <Icon
          name="md-add-circle"
          style={{ fontSize: 20, color: "#C6C6C6" }}
        ></Icon>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemAddBtn: {
    height: 100,
    borderColor: "#C6C6C6",
    borderWidth: 1,
    borderStyle: "dotted",
    borderRadius: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30
  }
});
